import json
import requests

from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_city_picture(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": f"{city} {state}"}
    url = "https://api.pexels.com/v1/search"

    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_city_weather(city, state):
    geocode_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state}&appid={OPEN_WEATHER_API_KEY}"
    geocode_response = requests.get(geocode_url)
    geocode_array = json.loads(geocode_response.content)
    lat = geocode_array[0]["lat"]
    lon = geocode_array[0]["lon"]
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    weather_response = requests.get(weather_url)
    weather_obj = json.loads(weather_response.content)

    try:
        return {
            "temp": weather_obj["main"]["temp"],
            "description": weather_obj["weather"][0]["description"],
        }
    except (KeyError, IndexError):
        return {
            "weather": None,
        }
